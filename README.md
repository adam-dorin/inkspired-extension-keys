# Inkspired Extension: Keyboard Shortcuts

#### This is a chrome extensions that provides some keyboard shortcuts for the tools and actions in https://inkarnate.com/

### Shortcuts:
*  Sculpt tool  `q`
*  Brush tool   `w`
*  Object tool  `e` 
*  Pattern tool `r` 
*  Text tool    `t` 
*  Note tool    `n` 
*  Grid tool    `g` 
*  Zoom tool    `z` 
*  Settings     `D`
*  Save map     `S`  
*  Export map   `E`

### TODO:
* Add actions for the top boxes (Place, Select, Scale, Panel Open)