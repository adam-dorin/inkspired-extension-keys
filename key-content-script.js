(function () {

    Object.prototype.forEach = function (callback) { // extension so that iteration over object is posible
        for (const key in this) {
            if (this.hasOwnProperty(key)) {
                const value = this[key];
                !!callback ? callback.apply(this, [value, key, this]) : null;
            }
        }
    };
    function DomReady(fn) {
        if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
          fn();
        } else {
          document.addEventListener('DOMContentLoaded', fn);
        }
      }
    function ButtonLoad(callback) { // tried a dom loaded but the buttons are not there when the event fires;
        var interval = setInterval(function () {
            if (document.querySelector('#export-action')) {
                clearInterval(interval);
                callback();
            }
        }, 500);
    };
    var Buttons = {
        tools: {},
        actions: {}
    };
    var tools = {
        113: 'sculpt', // q 113
        119: 'brush',  // w 119
        101: 'object', // e 101
        114: 'pattern',// r 114
        116: 'text',   // t 116
        110: 'note',   // n 110
        103: 'grid',   // g 103
        122: 'zoom',   // z 122
    };
    var actions = {
        68: 'settings',// D 68
        83: 'save',    // S 83 
        69: 'export'   // E 69
    };
    function setIconColor(){
        var keyboardChecker = document.querySelector('.keyboard-checker');
        if(!document.querySelector('#export-action') ){
            
            keyboardChecker.classList.remove('ready');
        } else{
            keyboardChecker.classList.add('ready')
        }
    }
    function keyboardInit() {

        tools.forEach(function (tool) {
            Buttons.tools[tool] = document.querySelector('li.' + tool);
        });

        actions.forEach(function (action) {
            Buttons.actions[action] = document.querySelector('#' + action + '-action');
        });
        // Listen for keypress and click 
        setIconColor();
        document.addEventListener('keypress', function (e) {

            var btnToClick;
            if (e.shiftKey) {
                btnToClick = Buttons.actions[actions[e.keyCode]];
            } else {
                btnToClick = Buttons.tools[tools[e.keyCode]];
            }

            if (btnToClick) {
                btnToClick.click();
            }
        });
    }
    ButtonLoad(keyboardInit);
    
    // Visual cue to display if the keys have been set
    DomReady(function(){
        var span = document.createElement('span');
        [   
            'glyphicon', 
            'keyboard-checker',
            'glyphicon-bookmark'
        ].forEach(function(cssClass){ span.classList.add(cssClass);  })
        
        document.querySelector('body').appendChild(span)
    });

    // support for site navigation
    window.addEventListener("hashchange", function(){       
        ButtonLoad(keyboardInit);
        setIconColor();
    })
})();